#!/bin/sh

set -e

echo "Clone lava.git"
git clone https://git.lavasoftware.org/lava/lava.git /root/lava
cd /root/lava
echo "done"
echo

echo "Check bullseye dependencies"
PACKAGES=$(./share/requires.py -p lava-server -d debian -s bullseye -n)
for pkg in $PACKAGES
do
  echo "* $pkg"
  dpkg -l "$pkg" > /dev/null
done

echo "Run server test suite"
.gitlab-ci/test/aarch64/server-debian-11.sh
echo "done"
