#!/bin/sh

service postgresql start
sudo -u postgres psql -c "CREATE ROLE devel NOSUPERUSER CREATEDB NOCREATEROLE INHERIT LOGIN ENCRYPTED PASSWORD 'devel'"

# Start the command or default to bash
exec "${@:-bash}"
